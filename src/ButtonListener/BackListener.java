package ButtonListener;

import it.sauronsoftware.ftp4j.*;
import service.FtpService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by а on 26.01.2015.
 */
public class BackListener implements ActionListener {

    FtpService clientFTP = new FtpService();
    DefaultListModel listModel = new DefaultListModel();

    public BackListener(FtpService clientFTP, DefaultListModel listModel) {
        this.clientFTP = clientFTP;
        this.listModel = listModel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        try {
            clientFTP.parentDir();
        } catch (FTPException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (FTPIllegalReplyException e1) {
            e1.printStackTrace();
        }
        listModel.clear();
        try {
            for (String s : clientFTP.filesNames())
                listModel.addElement(s);
        } catch (FTPAbortedException e1) {
            e1.printStackTrace();
        } catch (FTPDataTransferException e1) {
            e1.printStackTrace();
        } catch (FTPException e1) {
            e1.printStackTrace();
        } catch (FTPListParseException e1) {
            e1.printStackTrace();
        } catch (FTPIllegalReplyException e1) {
            e1.printStackTrace();
        }

    }
}
