package Form;

import service.Credentials;
import sun.java2d.pipe.RenderingEngine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class AuthenticationDialog extends JDialog {
    private JPanel contentPane=new JPanel();
    private JLabel loginLabel=new JLabel();
    private JLabel passwordLabel=new JLabel();
    private JTextField loginField=new JTextField();
    private JPasswordField passwordField=new JPasswordField();
    private JButton loginButton=new JButton();
    private Credentials credentials;

    public void RenderForm()
    {
        contentPane.setLayout(null);
        setContentPane(contentPane);
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocation(Toolkit.getDefaultToolkit().getScreenSize().width/2-150,Toolkit.getDefaultToolkit().getScreenSize().height/2-250);


        loginLabel.setText("Login");
        loginLabel.setSize(50,20);
        loginLabel.setLocation(10,10);
        contentPane.add(loginLabel);

        passwordLabel.setText("Password");
        passwordLabel.setSize(70,20);
        passwordLabel.setLocation(10,40);
        contentPane.add(passwordLabel);

        loginField.setSize(150,20);
        loginField.setLocation(80,10);
        contentPane.add(loginField);

        passwordField.setSize(150,20);
        passwordField.setLocation(80,40);
        contentPane.add(passwordField);

        loginButton.setSize(100,20);
        loginButton.setLocation(75, 70);
        loginButton.setText("Login");
        contentPane.add(loginButton);


    }



    public AuthenticationDialog()
    {
        RenderForm();
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                login();
                dispose();
            }
        });
    }



      public static void showDialog(Credentials credentials)
      {
          AuthenticationDialog dialog = new AuthenticationDialog();
          dialog.credentials = credentials;
          dialog.pack();
          dialog.setSize(250,130);
          dialog.setVisible(true);
      }
    public void login()
    {
        credentials.setUsername(loginField.getText());
        credentials.setPassword(new String(passwordField.getPassword()));


    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }
}
