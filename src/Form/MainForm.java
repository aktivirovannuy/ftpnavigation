package Form;

import ButtonListener.BackListener;
import it.sauronsoftware.ftp4j.*;
import service.FtpService;
//import service.FtpService;

import javax.swing.*;
import javax.swing.plaf.metal.MetalIconFactory;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

/**
 * Created by а on 15.01.2015.
 */
public class MainForm extends JFrame {
    private JButton cancelButton = new JButton();
    private JPanel rootPanel = new JPanel();
    private JButton openButton = new JButton();

    private JTextField fileNameField = new JTextField();
    private JLabel fileNameLabel = new JLabel();
    private JComboBox fileTypeComboBox = new JComboBox();
    private JLabel fileTypeLabel = new JLabel();
    private JButton conectionButton = new JButton();
    private JButton backButton = new JButton();
    private JLabel pathLabel = new JLabel();
    private JComboBox pathField = new JComboBox();
    private JButton homeButton = new JButton();
    private JButton newFolderButton = new JButton();
    private JButton listViewButton = new JButton();
    private JButton tableViewButton = new JButton();
    final DefaultListModel listModel = new DefaultListModel();
    final JList list = new JList(listModel);
    JScrollPane scroll = new JScrollPane(list);


    final FtpService clientFTP = new FtpService();

    public void RenderForm() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        rootPanel.setLayout(null);
        setContentPane(rootPanel);
        setResizable(false);
        setLocation(Toolkit.getDefaultToolkit().getScreenSize().width / 2 - 300, Toolkit.getDefaultToolkit().getScreenSize().height / 2 - 250);
        setVisible(true);
        pack();

        cancelButton.setSize(120, 30);
        cancelButton.setLocation(360, 330);
        cancelButton.setText("Cancel");
        rootPanel.add(cancelButton);

        openButton.setSize(120, 30);
        openButton.setLocation(230, 330);
        openButton.setIcon(MetalIconFactory.getFileChooserNewFolderIcon());
        openButton.setText("Open");
        rootPanel.add(openButton);

        pathField.setSize(260, 20);
        pathField.setLocation(70, 20);
        pathField.addItem("node0.net2ftp.ru");
        pathField.addItem("192.168.56.1");
        pathField.setSelectedItem(0);
        rootPanel.add(pathField);

        pathLabel.setSize(40, 20);
        pathLabel.setLocation(30, 20);
        pathLabel.setText("Path:");
        rootPanel.add(pathLabel);

        fileNameLabel.setSize(80, 20);
        fileNameLabel.setLocation(20, 270);
        fileNameLabel.setText("File:");
        rootPanel.add(fileNameLabel);

        fileNameField.setSize(300, 20);
        fileNameField.setLocation(100, 270);
        rootPanel.add(fileNameField);

        fileTypeLabel.setSize(80, 20);
        fileTypeLabel.setLocation(20, 300);
        fileTypeLabel.setText("File type:");
        rootPanel.add(fileTypeLabel);

        fileTypeComboBox.setSize(300, 20);
        fileTypeComboBox.setLocation(100, 300);
        rootPanel.add(fileTypeComboBox);

        conectionButton.setSize(20, 20);
        conectionButton.setLocation(340, 20);
        rootPanel.add(conectionButton);

        backButton.setSize(20, 20);
        backButton.setLocation(365, 20);
        backButton.setIcon(MetalIconFactory.getFileChooserUpFolderIcon());
        rootPanel.add(backButton);

        homeButton.setSize(20, 20);
        homeButton.setLocation(390, 20);
        homeButton.setIcon(MetalIconFactory.getFileChooserHomeFolderIcon());
        rootPanel.add(homeButton);

        newFolderButton.setSize(20, 20);
        newFolderButton.setLocation(415, 20);
        newFolderButton.setIcon(MetalIconFactory.getFileChooserNewFolderIcon());
        rootPanel.add(newFolderButton);

        listViewButton.setSize(20, 20);
        listViewButton.setLocation(445, 20);
        listViewButton.setIcon(MetalIconFactory.getFileChooserListViewIcon());
        rootPanel.add(listViewButton);

        tableViewButton.setSize(20, 20);
        tableViewButton.setLocation(465, 20);
        tableViewButton.setIcon(MetalIconFactory.getFileChooserDetailViewIcon());
        rootPanel.add(tableViewButton);

        list.setSelectedIndex(0);
        list.setFocusable(false);
        list.setSize(450, 210);
        list.setLocation(20, 50);
        list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        rootPanel.add(list);

        scroll.setSize(450, 210);
        scroll.setLocation(20, 50);
        rootPanel.add(scroll);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

    }

    public MainForm() {
        super("Open File");

        RenderForm();


        list.setCellRenderer(new DefaultListCellRenderer() {
            public Component getListCellRendererComponent(JList list,
                                                          Object value, int index, boolean isSelected,
                                                          boolean cellHasFocus) {
                Component component = super.getListCellRendererComponent(list,
                        value, index, isSelected, cellHasFocus);
                JLabel label = (JLabel) component;
                try {
                    if (clientFTP.isFolder(index))
                        label.setIcon(MetalIconFactory.getFileChooserNewFolderIcon());
                    else label.setIcon(MetalIconFactory.getTreeLeafIcon());
                } catch (FTPException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (FTPDataTransferException e) {
                    e.printStackTrace();
                } catch (FTPListParseException e) {
                    e.printStackTrace();
                } catch (FTPIllegalReplyException e) {
                    e.printStackTrace();
                } catch (FTPAbortedException e) {
                    e.printStackTrace();
                }
                return label;
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();

            }
        });

        openButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                int ret = fileopen.showDialog(null, "Открыть файл");

            }
        });

        conectionButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                try {
                    clientFTP.connectServer(pathField.getSelectedItem().toString());

                } catch (FTPIllegalReplyException e1) {
                    e1.printStackTrace();
                } catch (FTPException e1) {
                    e1.printStackTrace();
                }

                try {
                    for (String s : clientFTP.filesNames())
                        listModel.addElement(s);
                } catch (FTPAbortedException e1) {
                    e1.printStackTrace();
                } catch (FTPDataTransferException e1) {
                    e1.printStackTrace();
                } catch (FTPException e1) {
                    e1.printStackTrace();
                } catch (FTPListParseException e1) {
                    e1.printStackTrace();
                } catch (FTPIllegalReplyException e1) {
                    e1.printStackTrace();
                }


            }
        });

        list.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 1) {

                    try {
                        if (clientFTP.isFolder(list.getSelectedIndex()))
                            clientFTP.changeDIR(list.getSelectedValue().toString());
                        else
                            JOptionPane.showMessageDialog(null, "File");


                    } catch (Exception e1) {
                        e1.printStackTrace();
                        ;
                    }

                    listModel.clear();
                    try {
                        for (String s : clientFTP.filesNames())
                            listModel.addElement(s);
                    } catch (FTPAbortedException e1) {
                        e1.printStackTrace();
                    } catch (FTPDataTransferException e1) {
                        e1.printStackTrace();
                    } catch (FTPException e1) {
                        e1.printStackTrace();
                    } catch (FTPListParseException e1) {
                        e1.printStackTrace();
                    } catch (FTPIllegalReplyException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        backButton.addActionListener(new BackListener(clientFTP, listModel));
        homeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


            }
        });

    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
