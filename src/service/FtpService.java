package service;

import it.sauronsoftware.ftp4j.*;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by а on 18.01.2015.
 */
public class FtpService {

    private FTPClient ftpClient = new FTPClient();

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    private Credentials credentials = new Credentials();


    public void connectServer(String server) throws FTPIllegalReplyException, FTPException {

        int port = 21;


        try {
            ftpClient.connect(server);
            //  service.Credentials credentials = new service.Credentials();
            Form.AuthenticationDialog.showDialog(credentials);
            setCredentials(credentials);
            System.err.println(credentials.getUsername());
            System.err.println(credentials.getPassword());
            ftpClient.login(credentials.getUsername(), credentials.getPassword());
            boolean success = ftpClient.isAuthenticated();
            if (!success) {
                System.out.println("Could not login to the server");
                return;
            } else {
                System.out.println("LOGGED IN SERVER");
            }
        } catch (IOException ex) {
            System.out.println("Oops! Something wrong happened");
            ex.printStackTrace();
        }

    }

    public List<String> filesNames() throws FTPAbortedException, FTPDataTransferException, FTPException, FTPListParseException, FTPIllegalReplyException {
        List<String> filesList = new ArrayList<String>();
        try {
            it.sauronsoftware.ftp4j.FTPFile[] list = ftpClient.list();
            //it.sauronsoftware.ftp4j.FTPFile[] ftpFiles=ftpClient.list();
            filesList.clear();
            for (it.sauronsoftware.ftp4j.FTPFile s : list)
                filesList.add(s.getName());
        } catch (IOException ex) {
            System.out.println("Oops! Something wrong happened");
            ex.printStackTrace();
        }
        return filesList;
    }

    public void changeDIR(String path) throws FTPException, IOException, FTPIllegalReplyException {

        ftpClient.changeDirectory(path);


    }

    public String currentDir() throws FTPException, IOException, FTPIllegalReplyException {
        String s = ftpClient.currentDirectory();
        return s;
    }

    public void parentDir() throws FTPException, IOException, FTPIllegalReplyException {
        ftpClient.changeDirectoryUp();
    }

    public boolean isFolder(int i) throws FTPException, IOException, FTPDataTransferException, FTPListParseException, FTPIllegalReplyException, FTPAbortedException {
        int k;
        it.sauronsoftware.ftp4j.FTPFile[] list = ftpClient.list();
        k = list[i].getType();
        if (k == 1)
            return true;
        else
            return false;


    }


}

